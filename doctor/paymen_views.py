from django.http import Http404
from django.views.generic import View
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from doctor.payments import Payment
from doctor.models import PaymentModel

class DoctorRegisterPayment(View):
    def get(self, request, *args, **kwargs):
        pay = Payment(
            amount=8100,
            description="Qeydiyyat"
        )
        base_obj = PaymentModel(
            user=request.user,
            reference=pay.reference,
            prize=81,
            description=pay.description,
        )
        base_obj.save()
        return redirect(pay.run())

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DoctorRegisterPayment, self).dispatch(*args, **kwargs)



class BasePaymentCheckView(View):
    error_template = 'accounts/success/success_index.html'
    def get(self, request):
        if request.GET.get('reference'):
            base = PaymentModel.objects.get(user=request.user,reference=request.GET.get('reference'))
            base.done = True
            pay = Payment(amount=8100,description="Qeydiyyat")
            base_obj = pay.check_status(request.GET.get('reference'))
            base.data = base_obj['data']
            base.status = base_obj['status']
            base.save()
            if base.status:
                return redirect(reverse('base:dashboard-payment-success'))
            else:
                messages.success(
                    request, 'Hörmətli %s Ödənişiniz uğursuz alındı' % request.user.get_full_name()
                )
                return render(request, self.error_template)
        else:
            messages.success(
                request, 'Hörmətli %s Ödənişiniz uğursuz alındı' % request.user.get_full_name()
            )
            return render(request, self.error_template)


class SponsorPayment(View):
    def get(self, request):
        if request.GET.get('money'):
            pay = Payment(
                amount=int(request.GET.get('money')),
                description="Sponsored"
            )
            base_obj = PaymentModel(
                user=request.user,
                reference=pay.reference,
                prize=int(request.GET.get('money'))/100,
                description=pay.description,
            )
            base_obj.save()
            return redirect(pay.run())
        else:
            raise Http404


class UserMeetPayment(View):
    def get(self, request):
        if request.GET.get('money'):
            pay = Payment(
                amount=int(request.GET.get('money')),
                description="Gorush"
            )
            base_obj = PaymentModel(
                user=request.user,
                reference=pay.reference,
                prize=int(request.GET.get('money'))/100,
                description=pay.description,
            )
            base_obj.save()
            return redirect(pay.run())
        else:
            raise Http404